from __future__ import absolute_import
import unittest
import pprint
import sys
import os

p = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, p)

from lib import scrape

class TestScrape(unittest.TestCase):

    def test_get_url(self):
        url = r'https://en.wikipedia.org/wiki/Gilbert_Bayes'
        content = scrape.get_page(url)

        links = scrape.get_links(url, content, 'mw-content-text', 'en.wikipedia.org')
        self.assertTrue(r'https://en.wikipedia.org/wiki/Harry_Bates_(sculptor)' in links)

    def test_scrape_customfilters(self):
        scrape.DOMAIN_FILTERS = {'en.wikipedia.org': ['Wikipedia:']}
        url = r'https://en.wikipedia.org/wiki/Kent_St_Leger'
        content = scrape.get_page(url)
        links = scrape.get_links(url, content)
        self.assertTrue(r'https://en.wikipedia.org/wiki/Wikipedia:FAQ/Categorization' not in links)

    def test_mime_filter(self):
        url = r'https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3'
        r = scrape.get_page(url)
        self.assertIsNone(r)

if __name__ == '__main__':
    unittest.main()
