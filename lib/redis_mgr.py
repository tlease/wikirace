import redis
import sys
import os

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import config


def get_redis():
    return redis.StrictRedis(host=config.REDIS_SRV, port=config.REDIS_PORT,
                             db=config.REDIS_DB, password=config.REDIS_PW)
