import config
from lib import redis_mgr

redis_cxn = redis_mgr.get_redis()


def instruct_workers(command):
    if command.lower() not in ['stop', 'kill']:
        return False  # invalid command
    redis_cxn.publish('instruction', command)
