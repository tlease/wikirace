import json
import math
import random
from datetime import datetime

import redis_mgr
import scrape

redis_cxn = redis_mgr.get_redis()


def is_url_visited(url):
    return redis_cxn.sismember('visited_pages', url)


def set_url_visited(url):
    return redis_cxn.sadd('visited_pages', url)


def pages_visited():
    return redis_cxn.scard('visited_pages')


def incr_pages_seen(n):
    return redis_cxn.incrby('seen_pages', n)


def get_pages_seen():
    return redis_cxn.get('seen_pages')


def clear_work_tables_queue():
    r = redis_cxn.delete('visited_pages')
    r = redis_cxn.delete('seen_pages')
    r = redis_cxn.delete('start_url')
    r = redis_cxn.delete('target_url')


def clean_start():
    r = redis_cxn.delete('success')
    r = redis_cxn.delete('seen_pages')
    r = redis_cxn.delete('visited_pages')


def set_start_url(url):
    return redis_cxn.set('start_url', url)


def get_start_url():
    return redis_cxn.get('start_url')


def set_target_url(url):
    return redis_cxn.set('target_url', url)


def get_target_url():
    return redis_cxn.get('target_url')


def set_success(result_data):
    redis_cxn.set('success', json.dumps(result_data))


def get_success():
    # return is a dict
    res = redis_cxn.get('success')
    if res:
        return json.loads(res)
    return None


# below code is unused in current impl

def pop_next_from_work_queue(timeout=5):
    r = redis_cxn.blpop('work_queue', timeout)
    if not r:
        return None
    return json.loads(r[1])


def append_path_to_work_queue(url_array):
    """
    :param url_array: the path so far through the graph. next page on right end
    :return:
    """
    if not is_url_visited(url_array[-1]):
        redis_cxn.rpush('work_queue', json.dumps(url_array))


def upsert_page_info(page, outbound_edge_count=None):
    """
    Adds or updates a page info in the db. Not used for adding neighbors to graph.
    :param page: the url string for a page (stripped of query strings, params, frags)
    :param outbound_edge_count: optional int
    :return: int (the ID of the page)
    """
    id = get_or_create_url_id(page)
    url_info = {'id': id,
                'outbound_cnt': outbound_edge_count,
                'updated_at': datetime.utcnow().isoformat()}

    redis_cxn.hset('url_info', id, json.dumps(url_info))
    redis_cxn.hset('url_path_to_id', page, id)
    return url_info


def upsert_graph_by_url(url, links):
    id = get_or_create_url_id(url)
    return upsert_graph_by_id(id, links)


def upsert_graph_by_id(id, links):
    """
    Adds or refreshes neighbor info to cache graph for a node
    :param id: url id
    :param links: array of string links extracted from a page
    :return:
    """
    link_ids = get_url_ids(links)

    # fill in any missing IDs
    i = 0
    while True:
        i += 1
        if i > 100000:
            # TODO logging error
            break
        try:
            idx = link_ids.index(None)
            link_ids[idx] = create_url_id(links[idx])
        except ValueError:
            break
    res = redis_cxn.sadd('graph:%s' % id, *link_ids)


def get_neighbor_ids_by_id(id):
    return redis_cxn.smembers('graph:%s' % id)


def get_neighbor_ids_by_url(url):
    id = get_or_create_url_id(url)
    return get_neighbor_ids_by_id(id)


def get_neighbor_urls_by_id(id):
    ids = get_neighbor_ids_by_id(id)
    urls = get_url_paths(ids)
    return urls


def get_neighbor_urls_by_url(url):
    rootid = get_or_create_url_id(url)
    return get_neighbor_urls_by_id(rootid)


def get_url_ids(paths):
    """ paths is one or more url strings.
    returns a list of IDs.
    """
    ids = redis_cxn.hmget('url_path_to_id', paths)
    ids = [long(x) if x else None for x in ids]
    if len(ids) == 1:
        return ids[0]
    return ids


def get_url_paths(ids):
    """
    :param ids: one or more IDs
    :return: list of url path strings
    """
    if not ids:
        return []
    paths = redis_cxn.hmget('url_id_to_path', ids)
    if len(paths) == 1:
        return paths[0]
    return paths


def create_url_id(url):
    id = redis_cxn.incr('url_max')
    redis_cxn.hset('url_path_to_id', url, id)
    redis_cxn.hset('url_id_to_path', id, url)
    return id


def get_or_create_url_id(url):
    id = get_url_ids(url)
    if not id:
        id = create_url_id(url)
    return id


def get_url_info(id):
    info = redis_cxn.hget('url_info', id)
    return json.loads(info) if info else None


def append_path_to_weighted_queue(url_array):
    """

    :url_array: Path should be urls, do it in strings unless we have mem issues.
    :return: none
    """
    tail = url_array[-1]

    if is_url_visited(tail):
        return

    score = score_url(tail)
    if score == -1:  # page has no outgoing connections. don't bother queueing it
        return
    redis_cxn.zadd('weighted_queue', score, json.dumps(url_array))


def pop_weighted_queue():
    """
    Gets the highest ranked path for analysis. (Olog(n) time)
    redis has no zpop, so we may have contention issues here. since the keys are unique we can
    ignore errors where another thread poped the same key ahead & just try again.
    :return: an array
    """
    highest_key = redis_cxn.zrevrange('weighted_queue', 0, 0)
    if not highest_key:  # nothing in q
        return None

    res = redis_cxn.zrem('weighted_queue', highest_key[0])
    if res == 1:
        return json.loads(highest_key[0])
    else:  # another thread beat us to it, retry
        return pop_weighted_queue()


def score_url(url):
    """
    Get "connectivity" score of a node by normalizing the number of outgoing links from that node,
    and adding a random seed in order to allow some QoS but shuffling the lower scored nodes with higher. This
    is done to prevent starvation of low scored nodes  in cases where the target parth must go through a page with
    only a few outgoing links.

    If no connectivity weight for a node exists, we return a magic number that was found to be the media link count
    of 20k randomly chosen wiki pages.

    :param url: str url for one page
    :return:
    """
    no_score_link_count = 41

    id = get_url_ids(url)
    c = None
    if id:
        info = get_url_info(id)
        c = info.get('outbound_cnt') if info else None

    if not c:
        c = no_score_link_count

    return score(c)


def score(outgoing_link_count):
    # let c be the count of outgoing edges from an edges
    # if c == 0, return -1 (no outgoing pages. this is a dead end)
    # let weight be 1 - (1 / log2((c+1))) (0 < w < 1), shrinks total combinatoric space and
    if outgoing_link_count == 0:
        return -1
    if outgoing_link_count is None:
        outgoing_link_count = 41
    seed = random.random()
    weight = 1 - (1 / math.log((outgoing_link_count + 1), 2))
    return seed + weight


def path_is_valid(url_array):
    """
    adds a validation graph for a candidate result.

    :param url_array:
    :return:
    """
    for x in xrange(len(url_array) - 1):
        edge = url_array[x:x + 2]
        res = scrape.verify_link(edge[0], edge[1])
        if not res:
            return False
    return True
