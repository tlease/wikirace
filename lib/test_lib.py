from __future__ import absolute_import

import unittest
import random
import string
import json
import math
import sys
import os
from datetime import datetime

p = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, p)

from lib import redis_mgr
from lib import lib


def rand_str(n=50):
    return ''.join([random.choice(string.letters + string.digits) for x in xrange(n)])


class RedisTest(unittest.TestCase):
    @classmethod
    def setUp(cls):
        redis_mgr.get_redis().flushdb()

    @classmethod
    def tearDown(cls):
        redis_mgr.get_redis().flushdb()

    def test_set_page_visited(self):
        page = 'http://domain.com/page.html'
        lib.set_url_visited(page)
        self.assertTrue(lib.is_url_visited(page))

        page = 'http://domain.com/pagenotvisited.html'
        self.assertFalse(lib.is_url_visited(page))

    def test_create_url_id(self):
        page1 = rand_str()
        page2 = rand_str()
        id1 = lib.create_url_id(page1)
        id2 = lib.create_url_id(page2)

        self.assertEqual(id1, lib.get_url_ids(page1))
        self.assertEqual(id2, lib.get_url_ids(page2))
        self.assertEqual([id1, id2], lib.get_url_ids([page1, page2]))

        nopage = rand_str()
        self.assertEqual(None, lib.get_url_ids(nopage))

        self.assertEqual(page1, lib.get_url_paths(id1))
        self.assertEqual([page1, page2, None], lib.get_url_paths([id1, id2, 'nokey']))

    def test_get_or_create_url_id(self):
        path = rand_str()
        id = lib.get_or_create_url_id(path)
        self.assertEqual(path, lib.get_url_paths(id))

        id2 = lib.get_or_create_url_id(path)
        self.assertEqual(id, id2)

    def test_upsert_page_info(self):
        page = rand_str()
        id = lib.get_or_create_url_id(page)
        info = lib.upsert_page_info(page, 10)

        self.assertEqual(info['id'], id)
        self.assertEqual(info['outbound_cnt'], 10)

        fetched_info = lib.get_url_info(id)
        self.assertEqual(fetched_info['id'], id)
        self.assertEqual(fetched_info['outbound_cnt'], 10)
        dt = datetime.strptime(fetched_info['updated_at'], "%Y-%m-%dT%H:%M:%S.%f")
        now = datetime.utcnow()
        self.assertLess((now - dt).seconds, 10)

    def test_upsert_graph_by_id(self):
        page = rand_str()
        id = lib.get_or_create_url_id(page)
        existing_pages = [rand_str() for x in xrange(10)]
        existing_pages_ids = map(lib.get_or_create_url_id, existing_pages)
        new_pages = [rand_str() for x in xrange(10)]
        lib.upsert_graph_by_id(id, existing_pages + new_pages)

        neighbor_urls = lib.get_neighbor_urls_by_id(id)

        all = set(existing_pages).union(set(new_pages))
        self.assertEqual(all, set(neighbor_urls))

    def test_append_path_to_work_queue(self):
        path = ['a', 'b', 'c', 'd']
        lib.append_path_to_work_queue(path)
        path_out = lib.pop_next_from_work_queue()
        self.assertEqual(path, path_out)

        paths_in = []
        for x in xrange(10):
            path = [rand_str() for p in xrange(10)]
            lib.append_path_to_work_queue(path)
            paths_in.append(path)

        paths_out = []
        i = 0
        while True:
            i += 1
            if i > 100: break

            r = lib.pop_next_from_work_queue(timeout=1)
            if not r: break
            paths_out.append(r)
        self.assertEqual(paths_in, paths_out)

    def test_zset_add(self):
        lib.redis_cxn.delete('weighted_queue')

        path = [rand_str()]
        url = rand_str()
        path = path + [url]

        id = lib.get_or_create_url_id(url)
        links = [str(x) for x in range(0, 10)]
        lib.upsert_graph_by_id(id, links)
        lib.upsert_page_info(url, 10)
        lib.append_path_to_weighted_queue(path)
        info = lib.get_url_info(id)

        self.assertEqual(lib.redis_cxn.zcard('weighted_queue'), 1)
        item = lib.redis_cxn.zrevrange('weighted_queue', 0, 0)
        self.assertEqual(len(item), 1)
        self.assertEqual(json.loads(item[0]), path)

    def test_zpop(self):
        lib.redis_cxn.delete('weighted_queue')

        lib.redis_cxn.zadd('weighted_queue', 1, json.dumps('one'))
        lib.redis_cxn.zadd('weighted_queue', 2, json.dumps('two'))
        lib.redis_cxn.zadd('weighted_queue', 3, json.dumps('three'))

        item = lib.pop_weighted_queue()
        self.assertEqual(item, 'three')

    def skip_test_scores(self):
        link_counts = [0, 1, 10, 100, 1000]
        score_matrix = []
        # scoring involves fuzzing, so we run it a bunch of times & check that on average high scores are higher
        for x in xrange(100):
            score_matrix.append(map(lib.score, link_counts))

        avgs = []
        for i in xrange(len(score_matrix[0])):
            col = [x[i] for x in score_matrix]
            avgs.append(sum(col) / float(len(col)))

        avgs_sort = sorted(avgs)
        self.assertEqual(avgs, avgs_sort)


if __name__ == '__main__':
    unittest.main()
