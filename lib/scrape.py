import requests
import urlparse
import sys
from lxml import html


# Domain filters can be applied to exclude certain conventions or subpaths within a domain
# For example -- wikipedia pages all contain boilerplate links to pages that are never
# relevant to our search (like edit history, terms and conditions, etc.)
# Any url containing the filters will be runed from the search tree.

# DOMAIN_FILTERS = {'en.wikipedia.org': ['Wikipedia:']}
DOMAIN_FILTERS = {}

# DIV FILTERS
DIV_FILTERS = {'en.wikipedia.org': 'mw-content-text'}

# Often pages contain links to binary content that cannot contain hypertext.
# No point in downloading that stuff. The types below will be matched on all
# response Content-Type headers.
MIME_FILTERS = {'text/html', 'text/xml', 'text/x-sgml'}


# requests / link parsing below
# TODO remove hard coded div_id and domain and pass them in
def get_links(src_url, string_content, div_filter=True, match_domain=True):
    """

    :param src_url: the requested page we're extracting links from. Needed to expand relative links
    :param string_content: an html string
    :param div_filter: optional -- limit link extractions to a specific div in the page. This 
    is useful if you're aware of the page layout for a common domain and want to exclude sidebars, 
    footers, etc.
    :param match_domain: optional -- only return links to pages in the same subdomain. 
    Links from en.wikipedia.org/page/ will all have en.wikipedia.org as the netloc.
    :return: array of absolute urls, empty array if none
    """
    links = []
    try:
        domain = urlparse.urlparse(src_url).netloc
        div_to_filter = DIV_FILTERS.get(domain) if div_filter else None

        root = html.fromstring(string_content)
        div_filter_xpath = 'div[@id="{0}"]//'.format(div_to_filter) if div_to_filter else ''
        xpath_exp = r'//{0}a/@href'.format(div_filter_xpath)
        links = root.xpath(xpath_exp)

        # expand all relative links to absolute and
        # remove query strings, fragments, params
        for i, link in enumerate(links):
            full_path = urlparse.urljoin(src_url, link)
            parse = urlparse.urlparse(full_path)
            links[i] = parse.scheme + '://' + parse.netloc + parse.path

        if match_domain:
            links = filter(lambda x: domain in urlparse.urlsplit(x).netloc, links)
            filters = DOMAIN_FILTERS.get(domain, [])
            for _filter in filters:
                links = filter(lambda x: _filter not in x, links)
    except:
        print 'error parsing page %s' % src_url
        print sys.exc_info()

    return list(set(links))


def verify_link(from_page, to_page):
    from_content = get_page(from_page)
    links = get_links(from_page, from_content)
    return to_page in links


def get_page(url, timeout=5):
    """
    :param url: to be fetched
    :param timeout: request timeout
    :return: page's html if fetched, otherwise an exception or None
    """
    r = requests.get(url, timeout=timeout, stream=True)
    c_type = r.headers.get('Content-Type', '')
    if c_type.split(';')[0] not in MIME_FILTERS or \
            not r.ok:
        return None
    return r.content
