import multiprocessing
import Queue
import sys
import os
import time
import ctypes
import signal

import config
from monitor import InstructionMonitor
from lib import lib
from lib import scrape


def init_worker():
    signal.signal(signal.SIGINT, signal.SIG_IGN)

MAX_QUEUE_SIZE = 1000000
QUEUE_HEADSPACE = MAX_QUEUE_SIZE*.95
sync_lock = multiprocessing.Lock()


def testworker(target, local_queue, result, exit_flag):
    print 'entered %s' % os.getpid()
    i = 0
    for x in xrange(10):
        i += 1
        if exit_flag.value:
            break
        try:
            item = local_queue.get(timeout=1)
            # print item
        except Queue.Empty:
            continue
        except:
            break
        for y in xrange(10):
            try:
                local_queue.put([x] + [y])
            except Queue.Full:
                exit_flag.value = True
                break
    return i


def worker(target, local_queue, result, exit_flag):
    starving = False  # indicator for full queue. we need to start pruning branches here!
    pages_visited = 0
    pages_seen = 0
    branches_pruned = 0

    while True:
        pages_visited += 1
        if exit_flag.value:
            break
        try:
            path = local_queue.get(timeout=1)
        except Queue.Empty:
            continue
        except:
            print 'exception in worker queue.get'
            print sys.exc_info()
            break

        # 1. Pull a path off the queue
        # 2. Check if this page has been visited already, if so, skip it.
        # 3. Visit the page, extract the links
        # 3b. Mark the page visited in redis
        # 4. if the target is in the neighbors, add resuls  and exit
        # 5. check if we're starving the queue and skip additoinal queueing if so
        # 6. queue up neighbors

        this_page = path[-1]
        if lib.is_url_visited(this_page):
            continue
        lib.set_url_visited(this_page)

        content = scrape.get_page(this_page)
        neighbors = []
        if content:
            neighbors = scrape.get_links(this_page, content, config.DIV_FILTER, config.MATCH_DOMAIN)
        pages_seen += len(neighbors)
        lib.incr_pages_seen(len(neighbors))

        if target in set(neighbors):
            result.value = path+[target]
            exit_flag.value = True
            break

        if starving:
            if local_queue.qsize() > QUEUE_HEADSPACE:
                branches_pruned += len(neighbors)
                continue  # prune neighbor paths until we're at safe space
            else:
                starving = False

        for neighbor in neighbors:
            try:
                local_queue.put(path+[neighbor], timeout=1)
            except Queue.Full:
                branches_pruned += len(neighbors)
                starving = True
                break

    return pages_visited, pages_seen, branches_pruned


class RunWorkerPool(object):
    def __init__(self, manager, *args, **kwargs):
        """
        nproc: number of workers to run
        work_queue: shared memory queue to send work
        result: shared memory string to communicate result
        exit_flag
        """
        self._manager = manager
        self.work_queue = manager.Queue(maxsize=1000000)
        self.result = manager.Value(ctypes.c_char_p, '')
        self.exit_flag = manager.Value(ctypes.c_bool, False)
        self.nproc = config.NPROC
        self.pool_results = []
        self.start_time = 0
        self.finish_time = 0
        self.pool = None

        self._monitor = InstructionMonitor(self.exit_flag)
        self._monitor.setDaemon(True)
        self._monitor.start()

    def run_pool(self, start_page, target):
        # makes the pool and loops waiting for it workers to complete
        print 'Starting %s workers' % config.NPROC
        self.start_time = time.time()
        self.work_queue.put([start_page])
        self.pool = multiprocessing.Pool(self.nproc, initializer=init_worker)
        self.pool_results = [self.pool.apply_async(worker, args=(target, self.work_queue, self.result, self.exit_flag))
                             for x in xrange(self.nproc)]
        self.pool.close()
        return self.pool_results

    def workers_done(self):
        res = len([x for x in self.pool_results if x.ready()]) == len(self.pool_results)
        if res:
            self.finish_time = time.time()
        return res

    def wait_for_results(self, timeout=60 * 60 * 4):
        t0 = time.time()
        while not (self.workers_done() or time.time() > t0 + timeout):
            if self.exit_flag.value:
                break
            time.sleep(0.1)
            yield
        self.stop_workers()
        self.finish_time = time.time()
        yield [x.get() for x in self.pool_results if x.ready()]

    def stop_workers(self):
        self.exit_flag.value = True
        self.pool.join()

    def reset_pool(self):
        if self.pool:
            self.pool.terminate()
        self.pool_results = []
        self.start_time = 0
        self.finish_time = 0
        self.exit_flag.value = False

    def get_result_path_and_time(self):
        return self.result.value, self.finish_time-self.start_time

    def visited_pages(self):
        return lib.pages_visited()

