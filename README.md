# Wiki Racing POC

## Intro

Wiki racing is a race where, when given a starting and destination page,
an application traverses through wiki pages link by link to determine
the fastest route.

Fastest wall clock time is best.

## Reqs

* Python >=2.7
* Redis >=3.2

## Quick Guide
### AWS hosted version 
The app has been distributed to a trio of AWS servers that are available 
for ssh and execution.

Host | IP | DNS | Type  
wikirace01 | 54.202.212.242 | wikirace01.tlease.crabdance.com | t2.micro     
wikirace02 | 34.210.75.229 | wikirace02.tlease.crabdance.com | t2.micro   
redis01 | 34.209.152.102 | redis01.tlease.crabdance.com | t2.medium   

The user `wikirace` can ssh to the wikirace app servers (password sent separately).

When on the server, the app is installed to `/usr/local/wikirace` and should 
be runnable from that location following the usage guidelines below.

### Installation
1. Redis options: 
  - see [Quickstart](https://redis.io/topics/quickstart)
  - or [dockerize it](https://hub.docker.com/_/redis/)  
   `docker pull redis`

1. Configuration.  
Update the application's `./config.py` file. with the redis host
 IP and port. If the Redis server requires auth, this can be supplied
in the config file, or in a system environment variable named `REDIS_PW`.

1. Install python runtime reqs on workers.
  - Highly recommend isolating python to a virtualenv  
  `sudo apt-get install virtualenv`  
  `virtualenv wikirace_env`
  `source ./wikirace_env/bin/activate`
  
1. Run pip to install prereq libs:  
`pip install -r requirements.txt`

That should be all that is required.

### Running the app
*  Begin by starting as many workers as you can accommodate, this may 
be across many several machines. The default setting is for each worker 
process to start 8 subprocess workers. The number of subprocess workers is 
configurable in `config.py`  
Example: 

```python app.py --worker```  
  
For each command like this, a pool of subprocess workers will be created. This is 
reasonable to do once per host. Creating pools across many hosts is fine.

*  Next run another instance of `app.py` but provide options for `--start_url` and `--target_url` options.
 This will instruct all workers to begin work processing the graph.  
     
 ```python main.py --start_url="https://en.wikipedia.org/wiki/United_States" --target_url="https://en.wikipedia.org/wiki/Angina_pectoris" [--timeout=n]```  
     
 When the timeout is reached (5 minutes by default but overrideable with `--timeout` opt, or a path is found 
 the app wil exit, summarizing the runtime and found data, if any.

## Implementation

In order to maximize speed and parallelism, this application is designed to
be distributed horizontally. In order to accomplish this effectively,
a single source of truth is required to coordinate the graph traversal.
This is done with Redis in this implementation in order to strike a 
balance between implementation complexity and speed. (A single source of truth
for visited nodes is significantly simpler than partitioning nodes and balancing the 
workload between workers. However if the size of the cached graph
grows such that the store needs to scale out, a sharding approach would be 
implemented.) 

Many workers run in parallel. Several different algorithms were implemented
to compare, from a simple breadth first (BFS) to an approximation of A* with
a crude heuristic that weighs nodes by their connectivity.

The overall processing is similar, each worker:
1. Pulls start and target pages from Redis
1. Locates the neighbors of that node by visiting the page if it was not already done.
or by visiting the page.
1. Filters the neighbors for already visited, and queues new nodes to be visited by later

The app exits when the target page is found, or when a timeout is exceeded. Processes run
with the `--worker` switch will not exit automatically, instead they will wawit for the 
next request. At that time a new pool of subprocesses will be spawaned.

In order to limit network chatter and time waiting for responses from Redis, 
the subprocess pool of workers on each host use a shared memory queue to pass
results of page lookups. The subprocesses still consult Redis to determine if
the page has already been analyzed by a worker on another host however.
 


## Discussion
### Stack
The design decisions are a product as much of the constraints of 
the project as the task itself. I consider it a given that I don't 
have the resources available to hold an index of the graph of the
whole internet, or even wikipedia for that matter (wiki says they
have 5.38mm articles, each with 41 links per page on
average, making 2.205 billion edges to store just for wikipedia).
 
So if that's a given, an immediate tradeoff is apparant -- we'll need
to actually visit a lot of pages in the course of route finding. It won't
be highly likely that a path will be located in the cache alone. 
Here the workers will need to performing non-trivial tasks in parallel.
For this purpose I decided Redis is adequate for needs. If does not 
reqiure going to disk, handles lots of client connections out of the box,
and is quick to get up & running.
 
### Domain Analysis
First I implemented a simple brute force BFS method to get my feet wet.
Without even storing any graph data, I tasked up to 50 workers to
visit every page in the traversal path to find some test routes. Since
I had no idea how many edges a typical page has (or the shape of 
the graph at all) this was helpful. 

It was clear that most pages have a modest number of links (41 is 
the median), but there is a long tail of highly connected pages ("super nodes") 
with hundreds and even thousands of edges.

![Distribution of outbound edges](./data_analysis/outgoing_link_box.png)

### A dead end...
This tail of highly connected nodes seems like a good thing to exploit
for a limited cache of the graph. I hypothesized that this should 
represent "hot routes," and if I could index these high traffic nodes and 
get to them I could optimize by processing those parts of the graph.

In practice however, when searching for pages that aren't closely related
topically, this cache proved to be more trouble than it was worth (surprisingly 
to me). The overhead of storing and updating the cache was often greater than
the cost of downloading the page from the internet. (Code to perform the scoring
can be found still. See `score_url()` in [lib.py](./lib/lib.py)).
 
The tradeoffs here are: 
- A partial cache cannot be trusted. I still need to visit many pages
 to fill in what I don't have cached
- The cache must be built and maintained. Cleaned when dirty, pruned when too large, etc.
- If a requested path search includes uncommon pages it won't make
good use of the cache, and in fact the cache pages may lead us down 
a suboptimal route.
- Maintaining the cache may incur large enough overhead that in some cases
it is less performant than brute force BFS.
 
### Settling down on a search
After analyzing the performance on a couple AWS micro instances, I was able to 
saturate the CPU on the Redis side fairly quickly. This casued me to take a step back 
from relying on Redis fro the queue itself, and then later the cache. In this version
the best results for non-repetitive searches were achieved by a parallel BFS search
where each page is actually downloaded.

This was surprising initially -- but as I looked into the cache data it becaome
evident that the cache hit ratio was abysmal for any search criteria that didn't 
follow a very similar path to a previous search. This came at the expense of
overhead for all other searches, so I chose to remove the cache. This has the 
happy side effect of removing additoinal dependence on a single choke point
that's harder to scale than more app servers running more downloads in parallel.

 
## Shortcomings / Improvements to do
There are some obvious sohtcomings with the chosen approach.
- Speed. If wallclock speed is the measuring stick, we may need to cache a lot
of the graph up front and access it in a highly parallel fashion.
- Memory pressure. On small cloud instances Redis will be limited in size
before we can create a very large graph. Possible upgrade paths here a vertical
storage with a more mem for Redis, a typical RDBMS (say Postgres), or 
sacrificing latency for something like S3 as a backing k/v store.
- Store and model historical process requests to gauge performance and A/B test 
algo changes.
- Add web UI to submit and view requests instead of cli
