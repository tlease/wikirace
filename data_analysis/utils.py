import sys
import os
import sys
import requests
import pickle
import multiprocessing
import ctypes
import redis
import signal
import time
import base64
from datetime import datetime
from collections import Counter

p = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, p)

import config
from lib import lib
from lib import scrape

PROCS = 20
RANDOM_WIKI_URL = 'https://en.wikipedia.org/wiki/Special:Random'

def sample_random_wiki_page():
    r = requests.get(RANDOM_WIKI_URL)
    links = lib.get_links(r.url, r.content)
    #print 'processed %s from pid %s' % (r.url, os.getpid())
    return ({'url': r.url,
             'links_out': links,
             'resp_time': r.elapsed.microseconds / 1000.0})


def sample_random_wiki_pages(n):
    pool = multiprocessing.Pool(processes=PROCS)
    results = []
    for x in xrange(n):
        r = pool.apply_async(sample_random_wiki_page, ())
        results.append(r)

    for r in results:
        r.wait()

    res_vals = [r.get() for r in results if r.successful()]

    pickle.dump(res_vals, open('results.p', 'wb'))


def build_cache(tasks, exit, visited_cnt):
    red = redis.StrictRedis(host=config.REDIS_DB, password=config.REDIS_PW)
    red.delete('visited')
    starve = False
    while True:
        if exit:
            return
        try:
            path = tasks.get(timeout=1)
        except:
            break

        if starve and float(tasks.qsize()) / (tasks.maxsize()+1) < 0.9:
            starve = False

        this_page = path[-1]

        visited_cnt.value += 1
        content = scrape.get_page(this_page)
        links = scrape.get_links(this_page, content)

        key = 'graph:%s' % (base64.b64encode(this_page))
        p = red.pipeline()
        p.delete(key)
        p.sadd(key, *links)
        p.execute()

        if not starve:
            for link in links:
                try:
                    tasks.put(path+[link], timeout=1)
                except:
                    starve = True


def init_worker():
    signal.signal(signal.SIGINT, signal.SIG_IGN)


def progression():
    i = {'i': 0, 't0': time.time(), 'lastupdate': time.time()}
    def inner(cnt=None):
        i['i'] += 1
        now = time.time()
        if now-i['lastupdate'] > 30:
            i['lastupdaet'] = now
            print '...progression. Running time: {0}\tIteration: {1}\tCount: {2}'.format((now-i['t0']), i['i'], cnt )
    return inner

def run_cache_pool(procs=8):
    manager = multiprocessing.Manager()
    tasks = manager.Queue(100000)
    exit = manager.Value(ctypes.c_bool, False)
    visited = manager.Value(ctypes.c_int64, 0)
    prog = progression(visited.value)
    pool = multiprocessing.Pool(procs, initializer=init_worker)
    work_res = [pool.apply_async(build_cache, args=(tasks, exit, visited)) for x in xrange(procs)]
    pool.close()

    tasks.put([RANDOM_WIKI_URL])
    while True:
        prog(visited.value)
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            exit.value = True


if __name__ == '__main__':
    t1 = time.time()
    procs = []
    lib.redis_cxn.delete('visited_pages')
    lib.append_path_to_weighted_queue(['https://en.wikipedia.org/wiki/United_States'])

    pool = multiprocessing.Pool(4)
    res = [pool.apply_async(build_cache, args=(None, False, False)) for x in xrange(4)]
    pool.close()

    i = 0
    while len([x for x in res if not x.ready()]) > 0:
        i += 1
        if i % 100 == 0:
            v = lib.redis_cxn.scard('visited')
            k = len(lib.redis_cxn.keys())
            print '%s visited: %s keycount %s' % (datetime.now().isoformat(), v, k)
        time.sleep(3)

    pool.join()
    #build_cache(None, False, False)
