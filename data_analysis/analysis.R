require(ggplot2)

setwd('/sandbox/cookbooks/wikirace/data_analysis')

linksOut = read.csv("links_out.txt", col.names = c("page", "links"), sep="\t")
p = ggplot(linksOut, aes(x="Pages", y=links))
p + geom_boxplot(aes(y=links)) +  
  scale_y_log10() + 
  theme_minimal() +
  ylab("Outgoing Links Count") +
  ggtitle("Frequency of outgoing links")



linksIn = read.csv("links_in.txt", col.names = c("page", "links"), sep="\t")
p = ggplot(linksIn, aes(x=links))
p + geom_density()
  scale_y_log10() + 
  scale_x_log10()
  theme_bw() +
  ylab("Incoming Links Count") +
  ggtitle("Frequency of incoming links")


response_times = read.csv("resps.txt", col.names = c("ms"), sep="\t")
p = ggplot(response_times, aes(x=c(""), y=ms))
p + geom_boxplot() + scale_y_log10()

boxplot(response_times$ms)

