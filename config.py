import os

REDIS_SRV = os.environ.get('REDIS_SRV', 'redis01.tlease.crabdance.com')
REDIS_PORT = 6379
REDIS_DB = 0
REDIS_PW = os.environ.get('REDIS_PW', '')

NPROC = 8

# Timeout value is how long to wait by default for workers to find a path before giving up. (in seconds)
GLOBAL_TIMEOUT = 60*60

########################
# Link scraping settings
########################

# DIV_FILTER controls whether to load a predefined list of div name fliters (by domain).
# This is useful to exlude page parts like nav bars, etc.
# For example, en.wikipedia.org has the majority of relevant page content in the div #mw-content-text
DIV_FILTER = True

# MATCH_DOMAIN controls if the links extracted will remove any links where the netloc doesn't match the source page
# For example, extracting links from a page at en.wikipedia.org will only return other links to en.wikipedia.org.
MATCH_DOMAIN = True
