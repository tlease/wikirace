import threading
import time
import redis

import config
from lib import redis_mgr


class InstructionMonitor(threading.Thread):
    def __init__(self, exit_flag):
        super(InstructionMonitor, self).__init__()
        self.exit_flag = exit_flag
        self.redis_cxn = redis_mgr.get_redis()
        self.pubsub = self.redis_cxn.pubsub()
        self.pubsub.subscribe('instruction')

    def run(self):
        while True:
            msg = self.pubsub.get_message()
            if msg:
                data = str(msg['data']).lower()
                if data != '1': print data
                if data == 'stop' or data == 'kill':  # instructs worker
                    self.exit_flag.value = True
                    self.teardown()
                    break
                else:
                    print data
            time.sleep(1)

    def teardown(self):
        self.pubsub.unsubscribe()
