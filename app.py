import multiprocessing
import optparse
import sys
import time

import worker
import config
from lib import lib, instructor, redis_mgr


def parse_opts(args):
    parser = optparse.OptionParser()
    parser.add_option('--start_url', action='store', type='string', dest='start_url',
                      help='page to start')
    parser.add_option('--target_url', action='store', type='string', dest='target_url',
                      help='page to find')
    parser.add_option('--timeout', action='store', type='int', dest='timeout', default=5 * 60,
                      help='when to give up and declare no route found (seconds)')
    parser.add_option('--worker', action='store_true', dest='is_worker', default=False,
                      help='set if this instance should behave as a worker daemon')
    parser.add_option('--instruct', action='store', type='string', dest='command',
                      help='command to kill workers')

    (opts, args) = parser.parse_args(args)

    config.start_url = opts.start_url
    config.target_url = opts.target_url
    config.GLOBBAL_TIMEOUT = opts.timeout
    config.is_worker = opts.is_worker
    config.command = opts.command


def progression_closure(poll_seconds):
    now = time.time()
    data = {'t0': now,
            'lap': now}

    def inner():
        now = time.time()
        if now - data.get('lap') > poll_seconds:
            visited = lib.pages_visited()
            seen = lib.get_pages_seen()
            data['lap'] = now
            print '...progression: %s pages visited (%s seen), %s seconds running, %s visited pages/second' % \
                  (visited, seen, now - data.get('t0'), float(visited) / (now - data.get('t0')))

    return inner


progress = progression_closure(15)


def run_worker():
    runner = worker.RunWorkerPool(manager)

    while True:
        try:
            start = lib.get_start_url()
            target = lib.get_target_url()
            if not start and not target:
                time.sleep(1)
                continue

            print 'Looking for path from: %s to %s' % (start, target)
            runner.run_pool(start, target)
            for result_set in runner.wait_for_results():
                progress()

            visited = 0
            seen = 0
            pruned = 0
            for result in result_set:
                visited += result[0]
                seen += result[1]
                pruned += result[2]
            path, runtime = runner.get_result_path_and_time()

            results = {'path': path,
                       'runtime': runtime,
                       'visited': visited,
                       'seen': seen,
                       'pruned': pruned}
            instructor.instruct_workers('kill')
            lib.set_success(results)
            report_success(results)
            lib.clear_work_tables_queue()
            runner.reset_pool()

        except KeyboardInterrupt:
            break


def report_success(results):
    visited = results.get('visited')
    runtime = results.get('runtime')
    seen = results.get('seen')
    pruned = results.get('pruned')
    path = results.get('path')

    print 'Done.'
    print 'Pages Visited: %s (%s/sec)' % (visited, float(visited) / runtime)
    print 'Pages Seen: %s (%s/sec)' % (seen, float(seen) / runtime)
    print 'Branches Pruned: %s' % pruned
    print 'Runtime: %s sec.' % runtime
    print ''
    for x in path:
        print x


def main():
    if config.command:
        instructor.instruct_workers(config.command)
        return

    lib.set_start_url(config.start_url)
    lib.set_target_url(config.target_url)

    t0 = time.time()
    try:
        while time.time() < t0 + config.GLOBBAL_TIMEOUT:
            res = lib.get_success()
            if not res:
                time.sleep(1)
                continue
            else:
                report_success(res)
                break
    except KeyboardInterrupt:
        instruct('kill')


def instruct(command):
    instructor.instruct_workers(command)


if __name__ == '__main__':
    redis_cxn = redis_mgr.get_redis()
    manager = multiprocessing.Manager()
    lib.clean_start()

    parse_opts(sys.argv[1:])

    if config.is_worker:
        run_worker()
    else:
        main()
